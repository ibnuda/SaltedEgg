{-# LANGUAGE RecordWildCards #-}
import           Application ()
import           Data.IORef
import           Foundation
import           Yesod.Core

main :: IO ()
main = do
  appIORef <- newIORef 12
  warp 3030 App {..}
