{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE ViewPatterns      #-}
module Foundation where

import           Data.IORef

import           Yesod.Core

data App = App
  { appIORef :: IORef Int
  }

mkYesodData
  "App"
  [parseRoutes|
    /              HomeR GET
    /global/#Int   GlobR GET
  |]

instance Yesod App

