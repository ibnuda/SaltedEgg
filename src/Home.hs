{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Home where

import           Foundation
import           Yesod.Core

getHomeR :: Handler Html
getHomeR =
  defaultLayout $ do
    setTitle "Minimal Multifile"
    [whamlet|
      <p>
        <a href=@{GlobR 6}> Add 6 to global variable.
    |]
