{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Add where

import           Data.IORef
import           Foundation
import           Yesod.Core

getGlobR :: Int -> Handler Html
getGlobR youreggs = do
  appIORef <- appIORef <$> getYesod
  myeggs <- liftIO $ readIORef appIORef
  liftIO $ writeIORef appIORef (myeggs + youreggs)
  defaultLayout
    [whamlet|
      <p>My Eggs: #{myeggs}
      <p>Added by your Eggs: #{youreggs + myeggs}
    |]
